First 30 Days

All Onboarding completed: General & Sales

Target Accounts Chosen: Valid reason on why these target accts. 

Listen in on 3 peer calls with customers

Understanding of Git

Familiarity with tools: sfdc, Google stack, Outreach...

Understanding of competitors

Elevator Pitch Down

Contributing

Set first discovery call with prospect

First 60 Days

Account Plans: Understanding of key initiative(s) at prospect accts and owners

Starting to build campaigns with SDR/BDR

Attend local event(s): DevOps Meetup

Understanding of major Use Cases

First Opportunity created

First 90 Days

First deal closed

Multiple opportunities at multiple accounts
